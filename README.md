# REST API 20-12-2021


local run  
`$ mvn spring-boot:run`

local run with profile  
`$ mvn spring-boot:run -D spring-boot.run.profiles=Development`

run final solution  
`$ java -jar target/rest_api-<version>.jar --spring.profiles.active=development`  

## build

clean target  
`$ mvn clean`

build  
`$ mvn package`

Docker build

`$ git clone git@gitlab.com:reform1/spring-rest-api.git`
`$ cd spring-rest-api`
`$ docker build -t spring-rest-api:latest .`

## Tests
`$ mvn test`

## Code coverage
`$ mvn verify`

## CI/CD

`$ sudo apk add gitlab-runner`

__production server__

sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "qFoC5DyFgykSpBVnsNyt" \
  --executor "shell" \
  --description "docker-lab-node1" \
  --tag-list "production_server_tag" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"

__development server__

sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "qFoC5DyFgykSpBVnsNyt" \
  --executor "shell" \
  --description "docker-lab-node2" \
  --tag-list "develop_server_tag" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"

  __test server__

sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "qFoC5DyFgykSpBVnsNyt" \
  --executor "docker" \
  --description "docker-lab-node2" \
  --tag-list "develop_server_docker" \
  --docker-image "maven:3-openjdk-17-slim" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"

  `gitlab-runner &`