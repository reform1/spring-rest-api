package pt.rumos.rest_api.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;

@Service
public class UserService {
    
    @Autowired
    private UserRepository repo;

    @ModelAttribute("users")
    public List<User> findAll(){
        return (List<User>) repo.findAll();
    }
}
