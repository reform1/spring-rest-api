package pt.rumos.rest_api.user;

import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    
    @Autowired
    private UserService service;
    
    @GetMapping("/user")
    public String getAllUsers(Model m){
        return new Gson().toJson(service.findAll());
    }

}
