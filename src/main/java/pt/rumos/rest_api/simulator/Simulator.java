package pt.rumos.rest_api.simulator;

public interface Simulator {
    
    public String getBlink();

    public String getCounter();

    public String getRandom();
}
