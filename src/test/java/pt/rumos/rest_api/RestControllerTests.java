package pt.rumos.rest_api;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class RestControllerTests {
    
    @Autowired
    RestApiController ctrl;

    @Test
    void testIsPresent(){
        assertNotNull(ctrl);
    }
}
